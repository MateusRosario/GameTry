package com;


import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

public class Frame extends JFrame implements KeyListener, MouseListener, MouseMotionListener {
    //States;==========
    private int Altura;
    private int Largura;
    private int FPS;
    private ArrayList keysTypedP1;
    private ArrayList keysTypedP2;
    private int posicaoMouseX;
    private int posicaoMouseY;
    private boolean clicado = false;
    //=================
    //Print;===========
    private Imprimir tela;
    //================

    public Frame(int Largura, int Altura, int FPS){
        //Doing sets of parameters;
        this.Altura = Altura;
        this.Largura = Largura;
        this.FPS = FPS;
        //========================
        //seting of JFrame;
        this.setSize(Largura, Altura);
        this.setTitle("Game");
        this.setUndecorated(true);
        this.setVisible(true);
        this.setLayout(null);
        this.setResizable(false);
        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.addKeyListener(this);
        this.addMouseListener(this);
        this.addMouseMotionListener(this);
        //==========================================

        //To start;================
        keysTypedP1 = new ArrayList();
        keysTypedP2 = new ArrayList();
        tela = new Imprimir(FPS, this);
        //=========================

        loop();
    }

    private void loop(){
        while (true){
            imprimir();
            try{
                Thread.sleep(1000/FPS);
            }catch (Exception e){
                System.out.println("Exeception was Finded in loop: " + e);
            }
        }
    }

    private void imprimir(){
        Graphics frame = this.getGraphics();
        frame.drawImage(tela.print(),0,0, Largura, Altura, this);
    }

    public ArrayList getKeysTypedP1() {
        return keysTypedP1;
    }

    public ArrayList getKeysTypedP2() {
        return keysTypedP2;
    }

    public int getPosicaoMouseX() {
        return posicaoMouseX;
    }

    public int getPosicaoMouseY() {
        return posicaoMouseY;
    }

    public boolean isClicado() {
        return clicado;
    }

    @Override
    public void keyTyped(KeyEvent keyEvent) {

    }

    @Override
    public void keyPressed(KeyEvent keyEvent) {
        System.out.println("asdasd: " + keyEvent.getKeyCode());
        if(keyEvent.getKeyCode() == KeyEvent.VK_UP || keyEvent.getKeyCode() == KeyEvent.VK_DOWN || keyEvent.getKeyCode() == KeyEvent.VK_LEFT || keyEvent.getKeyCode() == KeyEvent.VK_RIGHT || keyEvent.getKeyCode() == KeyEvent.VK_ENTER){
            if(!keysTypedP1.contains(keyEvent.getKeyCode())) {
                keysTypedP1.add(keyEvent.getKeyCode());
            }
        }else if(keyEvent.getKeyCode() == KeyEvent.VK_W || keyEvent.getKeyCode() == KeyEvent.VK_S || keyEvent.getKeyCode() == KeyEvent.VK_A || keyEvent.getKeyCode() == KeyEvent.VK_D || keyEvent.getKeyCode() == KeyEvent.VK_SPACE){
            if(!keysTypedP2.contains(keyEvent.getKeyCode())) {
                keysTypedP2.add(keyEvent.getKeyCode());
            }
        }
    }

    @Override
    public void keyReleased(KeyEvent keyEvent) {
        keysTypedP1.remove((Object) keyEvent.getKeyCode());
        keysTypedP2.remove((Object) keyEvent.getKeyCode());
        /*
        switch (keyEvent.getKeyCode()){
            case KeyEvent.VK_UP:
                keysTypedP1.remove((Object) KeyEvent.VK_UP);
                break;
            case KeyEvent.VK_DOWN:
                keysTypedP1.remove((Object) KeyEvent.VK_DOWN);
                break;
            case KeyEvent.VK_LEFT:
                keysTypedP1.remove((Object) KeyEvent.VK_LEFT);
                break;
            case KeyEvent.VK_RIGHT:
                keysTypedP1.remove((Object) KeyEvent.VK_RIGHT);
                break;
            case KeyEvent.VK_ENTER:
                keysTypedP1.remove((Object) KeyEvent.VK_ENTER);
                break;
            default:
                break;
        }
        */
    }

    @Override
    public void mouseClicked(MouseEvent mouseEvent) {

    }

    @Override
    public void mousePressed(MouseEvent mouseEvent) {
        clicado = true;
    }

    @Override
    public void mouseReleased(MouseEvent mouseEvent) {
        clicado = false;
    }

    @Override
    public void mouseEntered(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseExited(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseDragged(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseMoved(MouseEvent mouseEvent) {
        System.out.println("Mouse: (" + (Math.round((((float) mouseEvent.getX()) / Largura) * 1920)) + ", " + (Math.round(((float) mouseEvent.getY()) / Altura * 1080)) + ")");
        posicaoMouseX = Math.round((((float) mouseEvent.getX()) / Largura) * 1920);
        posicaoMouseY = Math.round((((float) mouseEvent.getY()) / Altura) * 1080);
    }

    public static void main(String[] args) {
        int Largura = 1280;
        int Altura = 720;
        int FPS = 60;
        Frame frame = new Frame(Largura, Altura, FPS);
    }
}