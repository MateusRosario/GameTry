package com;

import com.Menu.Menu;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import com.InGame.InGame;

public class Imprimir {
    //Stats Statics;===========
    public static final int MenuInicial = 0;
    public static final int MenuInGame = 1;
    public static final int Opcoes = 2;
    public static final int NewGame = 3;
    public static final int P1vcPc = 4;
    public static final int P1vsP2 = 5;

    //=========================

    //Stats;=============
    private int Largura = 1920;
    private int Altura = 1080;
    private int FPS;
    private int window;
    private Frame observer;
    //===================

    //Print;===================
    private BufferedImage tela;
    //========================

    //Windows;===============
    private Menu menuInicial;
    private InGame painelInGame;
    //=======================

    public Imprimir(int FPS, Frame observer){
        //Doing sets of parameters;
        this.observer = observer;
        this.FPS = FPS;
        setWindow(Imprimir.MenuInicial);
        //========================

        //To Start;==============================
        this.tela = new BufferedImage(Largura, Altura, BufferedImage.TYPE_INT_RGB);
        this.menuInicial = new Menu(5, true, new int[]{Imprimir.NewGame, Imprimir.P1vcPc, Imprimir.P1vsP2,-1},200, 50, 20,400,this ,observer);

        configMenuInicial();
        //========================================
    }

    public BufferedImage print(){
        switch (window){
            case MenuInicial:
                menuInicial.getPrint(tela, observer.getKeysTypedP1().isEmpty()? -1:(int) observer.getKeysTypedP1().get(0));
                break;
            case MenuInGame:
                break;
            case Opcoes:
                break;
            case NewGame:
                painelInGame.print(tela);
                break;
            case P1vcPc:
                painelInGame.print(tela);
                break;
            case P1vsP2:
                painelInGame.print(tela);
                break;
        }
        return tela;
    }

    public void setWindow(int window){
        this.window = window;
        switch (window){
            case MenuInGame:
            break;
            case Opcoes:
            break;
            case NewGame:
                painelInGame = new InGame(InGame.NewGame, observer);
            break;
            case P1vcPc:
                painelInGame = new InGame(InGame.P1vcPc, observer);
            break;
            case P1vsP2:
                painelInGame = new InGame(InGame.P1vsP2, observer);
            break;
        }
    }

    public void configMenuInicial(){
        menuInicial.setBotoes("Imagens/menu-novojogo.png", "Imagens/menu-pxpc.png", "Imagens/menu-pxp.png", "Imagens/menu-opcoes.png", "Imagens/menu-sair.png");
        menuInicial.setSelecteds("Imagens/menu-novojogo_S.png", "Imagens/menu-pxpc_S.png", "Imagens/menu-pxp_S.png", "Imagens/menu-opcoes_S.png", "Imagens/menu-sair_S.png");
        menuInicial.setClikeds("Imagens/menu-novojogo_C.png", "Imagens/menu-pxpc_C.png", "Imagens/menu-pxp_C.png", "Imagens/menu-opcoes_C.png", "Imagens/menu-sair_C.png");
        menuInicial.setFundo("Imagens/menu-fundo.png");
    }
}