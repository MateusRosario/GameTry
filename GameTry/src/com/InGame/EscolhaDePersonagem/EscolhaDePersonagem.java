package com.InGame.EscolhaDePersonagem;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;
import com.Frame;

public class EscolhaDePersonagem {
    //Lista dos quadros de escolha de personagem;========================
    private ArrayList<ArrayList> quadros = new ArrayList<ArrayList>();
    private ArrayList<ArrayList> selectedsP1 = new ArrayList<ArrayList>();
    private ArrayList<ArrayList> selectedsP2 = new ArrayList<ArrayList>();
    private ArrayList<ArrayList> clikedsP1 = new ArrayList<ArrayList>();
    private ArrayList<ArrayList> clikedsP2 = new ArrayList<ArrayList>();
    private ArrayList<ArrayList> imagemDemo = new ArrayList<ArrayList>();

    private int LarguraQuadros;
    private int AlturaQuadros;
    //===================================================================

    private ImageIcon fundo;
    
    //Stats;===========================================================
    private int Largura = 1920;
    private int Altura = 1080;
    private int espacoEntreAsColunas;
    private int espacoEntreAsLinhas;
    private int quadroSelecXP1 = 0;
    private int quadroSelecYP1 = 0;
    private int quadroSelecXP2 = 0;
    private int quadroSelecYP2 = 0;
    private int quadroClikedXP1 = -1;
    private int quadroClikedYP1 = -1;
    private int quadroClikedXP2 = -1;
    private int quadroClikedYP2 = -1;
    private int posicaoYparaImpressao;
    private int posicaoXparaImpressao;

    private int posicaoYparaImpressaoDemo1;
    private int posicaoXparaImpressaoDemo1;
    private int posicaoYparaImpressaoDemo2;
    private int posicaoXparaImpressaoDemo2;
    private int LarguraDemo;
    private int AlturaDemo;

    private int quantDeColunas;
    private int quantDeLinhas;
    private Frame observer;

    private int numColunaFinal;

    private boolean twoplayears;
    private boolean P1escolhendo;
    private boolean P2escolhendo;
    private boolean PCescolhendo;
    //=================================================================
    //Lista de Imagens dos personagens;==================================
    private ArrayList<ImageIcon> personagens = new ArrayList<ImageIcon>();
    //===================================================================

    public EscolhaDePersonagem(int quantDeLinhas, int quantDeColunas, int espacoEntreAsColunas, int espacoEntreAsLinhas, int posicaoXparaImpressao,  int posicaoYparaImpressao, int LarguraQuadros, int AlturaQuadros, boolean twoplayears, Frame observer){

        for (int i = 0; i < quantDeLinhas; i++) {
            quadros.add(new ArrayList<ImageIcon>());
            selectedsP1.add(new ArrayList<ImageIcon>());
            selectedsP2.add(new ArrayList<ImageIcon>());
            clikedsP1.add(new ArrayList<ImageIcon>());
            clikedsP2.add(new ArrayList<ImageIcon>());
            imagemDemo.add(new ArrayList<ImageIcon>());
        }
        this.quantDeLinhas = quantDeLinhas;
        this.quantDeColunas = quantDeColunas;
        this.espacoEntreAsColunas = espacoEntreAsColunas;
        this.espacoEntreAsLinhas = espacoEntreAsLinhas;
        this.posicaoXparaImpressao = posicaoXparaImpressao;
        this.posicaoYparaImpressao = posicaoYparaImpressao;
        this.LarguraQuadros = LarguraQuadros;
        this.AlturaQuadros = AlturaQuadros;

        this.posicaoYparaImpressaoDemo1 = 900;
        this.posicaoXparaImpressaoDemo1 = 30;
        this.posicaoYparaImpressaoDemo2 = 850;
        this.posicaoXparaImpressaoDemo2 = 1440;
        this.AlturaDemo = 800;
        this.LarguraDemo = 400;
        this.twoplayears = twoplayears;
        this.observer = observer;

        if(twoplayears){
            P1escolhendo = true;
            P2escolhendo = true;
            PCescolhendo = false;
        }else{
            P1escolhendo = true;
            P2escolhendo = false;
            PCescolhendo = false;
        }
    }

    public void setQuadros(String ... quadros) {
        int QD = 0;
        for (ArrayList quadro : this.quadros) {
            for (int x = 0; x < quantDeColunas && QD < quadros.length; x++) {
                quadro.add(new ImageIcon(getClass().getResource(quadros[QD])));
                QD++;
                numColunaFinal = x;
            }
        }
    }

    public void setSelectedsP1(String ... selectedsP1) {
        int QD = 0;
        for (ArrayList selected : this.selectedsP1) {
            for (int x = 0; x < quantDeColunas && QD < selectedsP1.length; x++) {
                selected.add(new ImageIcon(getClass().getResource(selectedsP1[QD])));
                QD++;
            }
        }
    }

    public void setSelectedsP2(String ... selectedsP2) {
        int QD = 0;
        for (ArrayList selected : this.selectedsP2) {
            for (int x = 0; x < quantDeColunas && QD < selectedsP2.length; x++) {
                selected.add(new ImageIcon(getClass().getResource(selectedsP2[QD])));
                QD++;
            }
        }
    }

    public void setClikedsP1(String ... clikedsP1) {
        int QD = 0;
        for (ArrayList cliked : this.clikedsP1) {
            for (int x = 0; x < quantDeColunas && QD < clikedsP1.length; x++) {
                cliked.add(new ImageIcon(getClass().getResource(clikedsP1[QD])));
                QD++;
            }
        }
    }

    public void setClikedsP2(String ... clikedsP2) {
        int QD = 0;
        for (ArrayList cliked : this.clikedsP2) {
            for (int x = 0; x < quantDeColunas && QD < clikedsP2.length; x++) {
                cliked.add(new ImageIcon(getClass().getResource(clikedsP2[QD])));
                QD++;
            }
        }
    }

    public void setImagemDems(String ... demonstracoes){
        int QD = 0;
        for (ArrayList demonstracao : this.imagemDemo) {
            for (int x = 0; x < quantDeColunas && QD < demonstracoes.length; x++) {
                demonstracao.add(new ImageIcon(getClass().getResource(demonstracoes[QD])));
                QD++;
            }
        }
    }

    public void setFundo(String fundo){
        this.fundo = new ImageIcon(getClass().getResource(fundo));
    }

    public BufferedImage print(BufferedImage tela){
        if(P1escolhendo || PCescolhendo) {
            actionSelectedP1();

            actionClikedP1();
        }

        if(P2escolhendo) {
            actionSelectedP2();

            actionClikedP2();
        }

        Graphics draw = tela.getGraphics();
        draw.drawImage(fundo.getImage(),0,0, Largura, Altura, observer);

        mouseSelec();

        if(twoplayears){
            printQuadrosP2(draw);

            printQuadrosP1(draw);
        }else if(P1escolhendo){
            printQuadrosP1(draw);
        }else{
            printQuadrosPC(draw);
        }
        return tela;
    }

    public void actionSelectedP1() {
        ArrayList P1k = observer.getKeysTypedP1();
        if(P1k.contains(KeyEvent.VK_UP)){
            quadroSelecYP1 += -1;
            if(quadroSelecYP1 <= -1){
                if(quadroSelecXP1 <= numColunaFinal){
                    quadroSelecYP1 = quantDeLinhas-1;
                }else{
                    quadroSelecYP1 = quantDeLinhas-2;
                }
            }
            P1k.remove(((Object) KeyEvent.VK_UP));
        }else if(P1k.contains(KeyEvent.VK_DOWN)){
            quadroSelecYP1 +=1;
            if(quadroSelecXP1 <= numColunaFinal){
                if(quadroSelecYP1 >= quantDeLinhas){
                    quadroSelecYP1 = 0;
                }
            }else{
                if(quadroSelecYP1 >= quantDeLinhas-1){
                    quadroSelecYP1 = 0;
                }
            }
            P1k.remove(((Object) KeyEvent.VK_DOWN));

        }else if(P1k.contains(KeyEvent.VK_LEFT)){
            quadroSelecXP1 += -1;
            if(quadroSelecXP1 <= -1){
                if(quadroSelecYP1 >= quantDeLinhas-1){
                    quadroSelecXP1 = numColunaFinal;
                }else{
                    quadroSelecXP1 = quantDeColunas - 1;
                }
            }
            P1k.remove(((Object) KeyEvent.VK_LEFT));

        }else if(P1k.contains(KeyEvent.VK_RIGHT)){
            quadroSelecXP1 += 1;
            if(quadroSelecYP1 >= quantDeLinhas - 1){
                if(quadroSelecXP1 > numColunaFinal){
                    quadroSelecXP1 = 0;
                }
            }else{
                if(quadroSelecXP1 >= quantDeColunas){
                    quadroSelecXP1 = 0;
                }
            }
            P1k.remove(((Object) KeyEvent.VK_RIGHT));
        }else if(P1k.contains(KeyEvent.VK_ENTER)){
            quadroClikedXP1 = quadroSelecXP1;
            quadroClikedYP1 = quadroSelecYP1;
        }
    }

    public void actionSelectedP2() {
        ArrayList P2k = observer.getKeysTypedP2();
        if(P2k.contains(KeyEvent.VK_W)){
            quadroSelecYP2 += -1;
            if(quadroSelecYP2 <= -1){
                if(quadroSelecXP2 <= numColunaFinal){
                    quadroSelecYP2 = quantDeLinhas-1;
                }else{
                    quadroSelecYP2 = quantDeLinhas-2;
                }
            }
            P2k.remove(((Object) KeyEvent.VK_W));
        }else if(P2k.contains(KeyEvent.VK_S)){
            quadroSelecYP2 +=1;
            if(quadroSelecXP2 <= numColunaFinal){
                if(quadroSelecYP2 >= quantDeLinhas){
                    quadroSelecYP2 = 0;
                }
            }else{
                if(quadroSelecYP2 >= quantDeLinhas-1){
                    quadroSelecYP2 = 0;
                }
            }
            P2k.remove(((Object) KeyEvent.VK_S));

        }else if(P2k.contains(KeyEvent.VK_A)){
            quadroSelecXP2 += -1;
            if(quadroSelecXP2 <= -1){
                if(quadroSelecYP2 >= quantDeLinhas-1){
                    quadroSelecXP2 = numColunaFinal;
                }else{
                    quadroSelecXP2 = quantDeColunas - 1;
                }
            }
            P2k.remove(((Object) KeyEvent.VK_A));

        }else if(P2k.contains(KeyEvent.VK_D)){
            quadroSelecXP2 += 1;
            if(quadroSelecYP2 >= quantDeLinhas - 1){
                if(quadroSelecXP2 > numColunaFinal){
                    quadroSelecXP2 = 0;
                }
            }else{
                if(quadroSelecXP2 >= quantDeColunas){
                    quadroSelecXP2 = 0;
                }
            }
            P2k.remove(((Object) KeyEvent.VK_D));
        }else if(P2k.contains(KeyEvent.VK_SPACE)){
            quadroClikedXP2 = quadroSelecXP2;
            quadroClikedYP2 = quadroSelecYP2;
        }
    }

    public void actionClikedP1(){
        ArrayList P1k = observer.getKeysTypedP1();
        if(quadroClikedYP1 != -1 && !P1k.contains(KeyEvent.VK_ENTER) && !observer.isClicado()){
            quadroClikedYP1 = -1;
            quadroClikedXP1 = -1;

            if(twoplayears){
                //Configura personagem escolhido por P1;
                //=====================================;

            }else if(P1escolhendo){
                //Configura personagem escolhido por P1
                //=====================================;

                P1escolhendo = false;
                PCescolhendo = true;
            }else{
                //Configura personagem escolhido para PC;
                //======================================
            }
        }
    }

    public void actionClikedP2(){
        ArrayList P2k = observer.getKeysTypedP2();
        if(quadroClikedYP2 != -1 && !P2k.contains(KeyEvent.VK_SPACE)){
            quadroClikedYP2 = -1;
            quadroClikedXP2 = -1;
        }
    }

    public void mouseSelec(){
        for(int y = 0; y < quadros.size() ; y++ ){
            for (int x = 0; x < quadros.get(y).size() ; x++) {
                if(mouseEntered(x,y)){
                    quadroSelecXP1 = x;
                    quadroSelecYP1 = y;
                    if(observer.isClicado()){
                        quadroClikedXP1 = quadroSelecXP1;
                        quadroClikedYP1 = quadroSelecYP1;
                    }
                }
            }
        }
    }

    public void printQuadrosP1(Graphics draw){
        for (int y = 0; y < quadros.size() ; y++) {
            for (int x = 0; x < quadros.get(y).size(); x++) {
                if(y == quadroClikedYP1 && x == quadroClikedXP1){
                    draw.drawImage(((ImageIcon) clikedsP1.get(y).get(x)).getImage(),posicaoXparaImpressao + (LarguraQuadros + espacoEntreAsColunas) * x,posicaoYparaImpressao + (AlturaQuadros + espacoEntreAsLinhas) * y, LarguraQuadros, AlturaQuadros, observer);
                    //Atencao ---> fazer sistema de escolha dupla;
                    draw.drawImage(((ImageIcon) imagemDemo.get(y).get(x)).getImage(), posicaoXparaImpressaoDemo1, posicaoYparaImpressaoDemo1 - AlturaDemo,LarguraDemo,AlturaDemo,observer);
                }else if(y == quadroSelecYP1 && x == quadroSelecXP1){
                    draw.drawImage(((ImageIcon) selectedsP1.get(y).get(x)).getImage(),posicaoXparaImpressao + (LarguraQuadros + espacoEntreAsColunas) * x,posicaoYparaImpressao + (AlturaQuadros + espacoEntreAsLinhas) * y, LarguraQuadros, AlturaQuadros, observer);
                    draw.drawImage(((ImageIcon) imagemDemo.get(y).get(x)).getImage(), posicaoXparaImpressaoDemo1, posicaoYparaImpressaoDemo1 - AlturaDemo,LarguraDemo,AlturaDemo,observer);
                }else if(!twoplayears){
                    draw.drawImage(((ImageIcon) quadros.get(y).get(x)).getImage(), posicaoXparaImpressao + (LarguraQuadros + espacoEntreAsColunas) * x,posicaoYparaImpressao + (AlturaQuadros + espacoEntreAsLinhas) * y, LarguraQuadros, AlturaQuadros, observer);
                }
            }
        }
    }

    public void printQuadrosP2(Graphics draw){
        for (int y = 0; y < quadros.size() ; y++) {
            for (int x = 0; x < quadros.get(y).size(); x++) {
                if(y == quadroClikedYP2 && x == quadroClikedXP2){
                    draw.drawImage(((ImageIcon) clikedsP2.get(y).get(x)).getImage(),posicaoXparaImpressao + (LarguraQuadros + espacoEntreAsColunas) * x,posicaoYparaImpressao + (AlturaQuadros + espacoEntreAsLinhas) * y, LarguraQuadros, AlturaQuadros, observer);
                    draw.drawImage(((ImageIcon) imagemDemo.get(y).get(x)).getImage(), posicaoXparaImpressaoDemo2, posicaoYparaImpressaoDemo2 - AlturaDemo,LarguraDemo,AlturaDemo,observer);
                }else if(y == quadroSelecYP2 && x == quadroSelecXP2){
                    draw.drawImage(((ImageIcon) selectedsP2.get(y).get(x)).getImage(),posicaoXparaImpressao + (LarguraQuadros + espacoEntreAsColunas) * x,posicaoYparaImpressao + (AlturaQuadros + espacoEntreAsLinhas) * y, LarguraQuadros, AlturaQuadros, observer);
                    draw.drawImage(((ImageIcon) imagemDemo.get(y).get(x)).getImage(), posicaoXparaImpressaoDemo2, posicaoYparaImpressaoDemo2 - AlturaDemo,LarguraDemo,AlturaDemo,observer);
                }else{
                    draw.drawImage(((ImageIcon) quadros.get(y).get(x)).getImage(), posicaoXparaImpressao + (LarguraQuadros + espacoEntreAsColunas) * x,posicaoYparaImpressao + (AlturaQuadros + espacoEntreAsLinhas) * y, LarguraQuadros, AlturaQuadros, observer);
                }
            }
        }
    }

    public void printQuadrosPC(Graphics draw){
        for (int y = 0; y < quadros.size() ; y++) {
            for (int x = 0; x < quadros.get(y).size(); x++) {
                if(y == quadroClikedYP1 && x == quadroClikedXP1){
                    draw.drawImage(((ImageIcon) clikedsP2.get(y).get(x)).getImage(),posicaoXparaImpressao + (LarguraQuadros + espacoEntreAsColunas) * x,posicaoYparaImpressao + (AlturaQuadros + espacoEntreAsLinhas) * y, LarguraQuadros, AlturaQuadros, observer);
                    draw.drawImage(((ImageIcon) imagemDemo.get(y).get(x)).getImage(), posicaoXparaImpressaoDemo2, posicaoYparaImpressaoDemo2 - AlturaDemo,LarguraDemo,AlturaDemo,observer);
                }else if(y == quadroSelecYP1 && x == quadroSelecXP1){
                    draw.drawImage(((ImageIcon) selectedsP2.get(y).get(x)).getImage(),posicaoXparaImpressao + (LarguraQuadros + espacoEntreAsColunas) * x,posicaoYparaImpressao + (AlturaQuadros + espacoEntreAsLinhas) * y, LarguraQuadros, AlturaQuadros, observer);
                    draw.drawImage(((ImageIcon) imagemDemo.get(y).get(x)).getImage(), posicaoXparaImpressaoDemo2, posicaoYparaImpressaoDemo2 - AlturaDemo,LarguraDemo,AlturaDemo,observer);
                }else if(!twoplayears){
                    draw.drawImage(((ImageIcon) quadros.get(y).get(x)).getImage(), posicaoXparaImpressao + (LarguraQuadros + espacoEntreAsColunas) * x,posicaoYparaImpressao + (AlturaQuadros + espacoEntreAsLinhas) * y, LarguraQuadros, AlturaQuadros, observer);
                }
            }
        }
    }

    public boolean mouseEntered(int x, int y){
        int X = observer.getPosicaoMouseX();
        int Y = observer.getPosicaoMouseY();

        return X > (posicaoXparaImpressao + (LarguraQuadros + espacoEntreAsColunas) * x) && X < ((posicaoXparaImpressao + (LarguraQuadros + espacoEntreAsColunas) * x) + LarguraQuadros) && Y > (posicaoYparaImpressao + (AlturaQuadros + espacoEntreAsLinhas) * y) && Y < ((posicaoYparaImpressao + (AlturaQuadros + espacoEntreAsLinhas) * y) + AlturaQuadros);
    }
}