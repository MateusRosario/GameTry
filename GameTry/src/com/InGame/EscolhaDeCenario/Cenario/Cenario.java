package com.InGame.EscolhaDeCenario.Cenario;

import com.Frame;

public class Cenario {

    private int Largura;
    private int Altura;
    private String fundo;
    private Frame observer;
    private Superficie[] superficies;

    public Cenario(int Largura, int Altura, String fundo, Frame observer,Superficie ... superficies){
        this.Largura = Largura;
        this.Altura = Altura;
        this.fundo = fundo;
        this.observer = observer;
        this.superficies = superficies;
    }
}
