package com.InGame.EscolhaDeCenario.Cenario;

import java.awt.*;

public class Superficie {
    private Point inicio;
    private Point termino;
    private int Altura;
    private boolean solida;

    public Superficie(Point inicio, Point termino, int Altura, boolean solida){
        this.inicio = inicio;
        this.termino = termino;
        this.Altura = Altura;
        this.solida = solida;
    }

    public Point getInicio() {
        return inicio;
    }

    public Point getTermino() {
        return termino;
    }

    public int getAltura() {
        return Altura;
    }

    public boolean isSolida() {
        return solida;
    }
}
