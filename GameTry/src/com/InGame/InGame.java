package com.InGame;

import com.Frame;
import com.InGame.EscolhaDePersonagem.EscolhaDePersonagem;

import java.awt.*;
import java.awt.image.BufferedImage;

public class InGame {
    //Stats Statics;===========
    public static final int NewGame = 0;
    public static final int P1vcPc = 1;
    public static final int P1vsP2 = 2;
    public static final int Cenario = 3;
    public static final int Game = 4;
    //=========================

    //Stats Statics;================
    public static final int telaEscolhaDePersonagem = 0;
    public static final int telaEscolhaDeCenario = 1;
    public static final int luta = 2;
    public static final int menuPause = 3;
    //==============================

    //Stats;==============
    private int window = 0;
    private int modo;
    private Frame observer;
    //====================

    EscolhaDePersonagem escolhaDePersonagem;

    public InGame(int modo, Frame observer){
        this.observer = observer;
        this.modo = modo;
        switch (modo){
            case InGame.NewGame:
                escolhaDePersonagem = new EscolhaDePersonagem(2, 4,10,10, 500, 100, 200, 200, false, observer);
                break;
            case InGame.P1vcPc:
                escolhaDePersonagem = new EscolhaDePersonagem(2, 4,10,10, 500, 100, 200, 200, false, observer);
                break;
            case InGame.P1vsP2:
                escolhaDePersonagem = new EscolhaDePersonagem(2, 4,10,10, 500, 100, 200, 200, true, observer);
                break;
            case InGame.Cenario:
                break;
            case InGame.Game:
                break;
        }
        configEscolhaDePersonagem();
    }

    public void configEscolhaDePersonagem(){
        escolhaDePersonagem.setQuadros("Imagens/quadro-naruto.png", "Imagens/quadro-naruto.png", "Imagens/quadro-naruto.png", "Imagens/quadro-naruto.png", "Imagens/quadro-naruto.png");
        escolhaDePersonagem.setSelectedsP1("Imagens/quadro-naruto_C.png", "Imagens/quadro-naruto_C.png", "Imagens/quadro-naruto_C.png", "Imagens/quadro-naruto_C.png", "Imagens/quadro-naruto_C.png");
        escolhaDePersonagem.setSelectedsP2("Imagens/quadro-naruto_S.png", "Imagens/quadro-naruto_S.png", "Imagens/quadro-naruto_S.png", "Imagens/quadro-naruto_S.png", "Imagens/quadro-naruto_S.png");
        escolhaDePersonagem.setClikedsP1("Imagens/quadro-naruto.png", "Imagens/quadro-naruto.png", "Imagens/quadro-naruto.png", "Imagens/quadro-naruto.png", "Imagens/quadro-naruto.png");
        escolhaDePersonagem.setClikedsP2("Imagens/quadro-naruto.png", "Imagens/quadro-naruto.png", "Imagens/quadro-naruto.png", "Imagens/quadro-naruto.png", "Imagens/quadro-naruto.png");
        escolhaDePersonagem.setImagemDems("Imagens/naruto-demo.png", "Imagens/naruto-demo.png", "Imagens/naruto-demo.png", "Imagens/naruto-demo.png", "Imagens/naruto-demo.png");
        escolhaDePersonagem.setFundo("Imagens/escolhaP-fundo.png");
    }

    public BufferedImage print(BufferedImage tela){
        switch (window){
            case telaEscolhaDePersonagem:
                    escolhaDePersonagem.print(tela);
                break;
            case telaEscolhaDeCenario:
                break;
            case luta:
                break;
            case menuPause:
                break;
        }
        return tela;
    }
}