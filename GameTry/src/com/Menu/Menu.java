package com.Menu;

import com.Imprimir;
import com.sun.org.apache.bcel.internal.generic.Select;

import com.Frame;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

public class Menu {
    //List of Images;========================
    private ArrayList<ImageIcon> botoes = new ArrayList<ImageIcon>();
    private ArrayList<ImageIcon> selecteds = new ArrayList<ImageIcon>();
    private ArrayList<ImageIcon> clikeds = new ArrayList<ImageIcon>();
    private ImageIcon fundo;
    private int LarguraBotoes;
    private int AlturaBotoes;
    //======================================
    //States;====================
    private int qtyBotoes;
    private int Largura = 1920;
    private int Altura = 1080;
    private int espacoEntreOsBotoes;
    private int posicaoYparaImpressao;
    private int botaoSelec = 0;
    private int botaoCliked = -1;
    private boolean botaoSair;
    private int actions[];
    private Frame observer;
    private Imprimir caller;
    //===========================
    public Menu(int qtyBotoes, boolean botaoSair, int actions[], int LarguraBotoes, int AlturaBotoes, int espacoEntreOsBotoes, int posicaoYparaImpressao, Imprimir caller, Frame observer){
        this.qtyBotoes = qtyBotoes;
        this.botaoSair = botaoSair;
        this.actions = actions;
        this.LarguraBotoes = LarguraBotoes;
        this.AlturaBotoes = AlturaBotoes;
        this.espacoEntreOsBotoes = espacoEntreOsBotoes;
        this.posicaoYparaImpressao = posicaoYparaImpressao;
        this.caller = caller;
        this.observer = observer;
    }

    public void setBotoes(String ... botoes){
        for (int i = 0; i < botoes.length; i++) {
            this.botoes.add(new ImageIcon(getClass().getResource(botoes[i])));
        }
    }

    public void setSelecteds(String ... botoesSelecteds){
        for (int i = 0; i < botoesSelecteds.length; i++) {
            this.selecteds.add(new ImageIcon(getClass().getResource(botoesSelecteds[i])));
        }
    }

    public void setClikeds(String ... botoesClikeds){
        for (int i = 0; i < botoesClikeds.length; i++) {
            this.clikeds.add(new ImageIcon(getClass().getResource(botoesClikeds[i])));
        }
    }

    public void setFundo(String fundo){
        this.fundo = new ImageIcon(getClass().getResource(fundo));
    }

    public BufferedImage getPrint(BufferedImage tela, int keyTyped){

        actionSelected(keyTyped);

        actionCliked(keyTyped);

        Graphics draw = tela.getGraphics();
        draw.drawImage(fundo.getImage(), 0, 0, Largura, Altura, observer);

        for (int i = 0; i < botoes.size() ; i++) {
            if(mouseEntered(i)){
                botaoSelec = i;
                if(observer.isClicado()){
                    botaoCliked = botaoSelec;
                }
            }
        }

        for (int i = 0; i < botoes.size() ; i++) {
            if(i == botaoCliked){
                draw.drawImage(clikeds.get(i).getImage(), Largura / 2 - LarguraBotoes / 2, posicaoYparaImpressao + (i * espacoEntreOsBotoes) + ((i - 1) * AlturaBotoes), LarguraBotoes, AlturaBotoes, observer);
            }else if(i == botaoSelec){
                draw.drawImage(selecteds.get(i).getImage(), Largura / 2 - LarguraBotoes / 2, posicaoYparaImpressao + (i * espacoEntreOsBotoes) + ((i - 1) * AlturaBotoes), LarguraBotoes, AlturaBotoes, observer);
            }else{
                draw.drawImage(botoes.get(i).getImage(), Largura / 2 - LarguraBotoes / 2, posicaoYparaImpressao + (i * espacoEntreOsBotoes) + ((i - 1) * AlturaBotoes), LarguraBotoes, AlturaBotoes, observer);
            }
        }
        return tela;
    }

    public void actionSelected(int keyTyped){
        switch (keyTyped){
            case KeyEvent.VK_UP:
                botaoSelec -= 1;
                if(botaoSelec <= -1){
                    botaoSelec = botoes.size()-1;
                }
                observer.getKeysTypedP1().removeAll(observer.getKeysTypedP1());
                break;
            case KeyEvent.VK_DOWN:
                botaoSelec += 1;
                if(botaoSelec >= botoes.size()){
                    botaoSelec = 0;
                }
                observer.getKeysTypedP1().removeAll(observer.getKeysTypedP1());
                break;
            case KeyEvent.VK_ENTER:
                botaoCliked = botaoSelec;
                break;
            case -1:
                break;
        }
    }

    public void actionCliked(int keyTyped){
        if(botaoCliked != -1 && keyTyped != KeyEvent.VK_ENTER && !observer.isClicado()){
            botaoCliked = -1;
            if(botaoSelec == qtyBotoes-1){
                observer.dispose();
            }

            for (int i = 0; i < qtyBotoes-1 ; i++) {
                if(botaoSelec == i){
                    caller.setWindow(actions[i]);
                }
            }
        }
    }

    public boolean mouseEntered(int i){
        int X = observer.getPosicaoMouseX();
        int Y = observer.getPosicaoMouseY();

        return X > (Largura / 2 - LarguraBotoes / 2) && X < ((Largura / 2 - LarguraBotoes / 2) + LarguraBotoes) && Y > (posicaoYparaImpressao + (i * espacoEntreOsBotoes) + ((i - 1) * AlturaBotoes)) && Y < ((posicaoYparaImpressao + (i * espacoEntreOsBotoes) + ((i - 1) * AlturaBotoes)) + AlturaBotoes);
    }
}